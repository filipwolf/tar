from torch import nn, torch
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from config import DEVICE


class RNNLayer(nn.Module):
    def __init__(self, input_size, rnn_size, num_layers, dropout, RNNType):
        """
        A simple RNN Encoder.

        Args:
            input_size (int): the size of the input features
            rnn_size (int):
            num_layers (int):
            dropout (float):

        Returns: outputs, last_outputs
        - **outputs** of shape `(batch, seq_len, hidden_size)`:
          tensor containing the output features `(h_t)`
          from the last layer of the LSTM, for each t.
        - **last_outputs** of shape `(batch, hidden_size)`:
          tensor containing the last output features
          from the last layer of the LSTM, for each t=seq_len.

        """
        super(RNNLayer, self).__init__()

        if RNNType == 'LSTM':
            self.lstm = nn.LSTM(input_size=input_size, hidden_size=rnn_size, num_layers=num_layers, bidirectional=True,
                                dropout=dropout, batch_first=True)
        elif RNNType == 'GRU':
            self.lstm = nn.GRU(input_size=input_size, hidden_size=rnn_size, num_layers=num_layers, bidirectional=True,
                               dropout=dropout, batch_first=True)
        elif RNNType == 'RNN':
            self.lstm = nn.RNN(input_size=input_size, hidden_size=rnn_size, num_layers=num_layers, bidirectional=True,
                               dropout=dropout, batch_first=True)
        else:
            print("Unknown rnn type")

        # the dropout "layer" for the output of the RNN
        self.drop_rnn = nn.Dropout(dropout)

        # define output feature size
        self.feature_size = rnn_size * 2

    @staticmethod
    def last_by_index(outputs, lengths):
        # Index of the last output for each sequence.
        idx = (lengths - 1).view(-1, 1).expand(outputs.size(0),
                                               outputs.size(2)).unsqueeze(1)
        return outputs.gather(1, idx).squeeze()

    @staticmethod
    def split_directions(outputs):
        direction_size = int(outputs.size(-1) / 2)
        forward = outputs[:, :, :direction_size]
        backward = outputs[:, :, direction_size:]
        return forward, backward

    def last_timestep(self, outputs, lengths):
        forward, backward = self.split_directions(outputs)
        last_forward = self.last_by_index(forward, lengths)
        last_backward = backward[:, 0, :]
        return torch.cat((last_forward, last_backward), dim=-1)

    def forward(self, embs, lengths):
        """
        This is the heart of the model. This function, defines how the data
        passes through the network.
        Args:
            embs (): word embeddings
            lengths (): the lengths of each sentence

        Returns: the logits for each class

        """
        # pack the batch
        packed = pack_padded_sequence(embs, list(lengths.data), batch_first=True)

        out_packed, _ = self.lstm(packed)

        # unpack output - no need if we are going to use only the last outputs
        outputs, _ = pad_packed_sequence(out_packed, batch_first=True)

        # get the outputs from the last *non-masked* timestep for each sentence
        last_outputs = self.last_timestep(outputs, lengths)

        # apply dropout to the outputs of the RNN
        last_outputs = self.drop_rnn(last_outputs)

        return outputs, last_outputs


class EmbeddingLayer(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, embeddings=None, dropout=.0, trainable=False):
        """
        Define the layer of the model and perform the initializations
        of the layers (wherever it is necessary)
        Args:
            embeddings (numpy.ndarray): the 2D ndarray with the word vectors
            dropout (float):
            trainable (bool):
        """
        super(EmbeddingLayer, self).__init__()

        # define the embedding layer, with the corresponding dimensions
        self.embedding = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim)

        if embeddings is not None:
            print("Initializing Embedding layer with pre-trained weights!")
            self.embedding.weight = nn.Parameter(torch.from_numpy(embeddings), requires_grad=trainable)

        # the dropout "layer" for the word embeddings
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        """
        This is the heart of the model. This function, defines how the data
        passes through the network.
        Args:
            x (): the input data (the sentences)

        Returns: the logits for each class

        """
        embeddings = self.embedding(x)

        if self.dropout.p > 0:
            embeddings = self.dropout(embeddings)

        return embeddings


class AttentionLayer(nn.Module):
    def __init__(self, attention_size, batch_first=False, layers=1, dropout=.0, non_linearity="tanh"):
        super(AttentionLayer, self).__init__()

        self.batch_first = batch_first

        if non_linearity == "relu":
            activation = nn.ReLU()
        else:
            activation = nn.Tanh()

        modules = []
        for i in range(layers - 1):
            modules.append(nn.Linear(attention_size, attention_size))
            modules.append(activation)
            modules.append(nn.Dropout(dropout))

        # last attention layer must output 1
        modules.append(nn.Linear(attention_size, 1))
        modules.append(activation)
        modules.append(nn.Dropout(dropout))

        self.attention = nn.Sequential(*modules)

        self.softmax = nn.Softmax(dim=-1)

    @staticmethod
    def get_mask(attentions, lengths):
        """
        Construct mask for padded itemsteps, based on lengths
        """
        max_len = max(lengths.data)
        mask = Variable(torch.ones(attentions.size())).detach()
        mask = mask.to(DEVICE)

        for i, l in enumerate(lengths.data):  # skip the first sentence
            if l < max_len:
                mask[i, l:] = 0
        return mask

    def forward(self, inputs, lengths):

        ##################################################################
        # STEP 1 - perform dot product
        # of the attention vector and each hidden state
        ##################################################################

        # inputs is a 3D Tensor: batch, len, hidden_size
        # scores is a 2D Tensor: batch, len
        scores = self.attention(inputs).squeeze()
        scores = self.softmax(scores)

        ##################################################################
        # Step 2 - Masking
        ##################################################################

        # construct a mask, based on sentence lengths
        mask = self.get_mask(scores, lengths)

        # apply the mask - zero out masked timesteps
        masked_scores = scores * mask

        # re-normalize the masked scores
        _sums = masked_scores.sum(-1, keepdim=True)  # sums per row
        scores = masked_scores.div(_sums)  # divide by row sum

        ##################################################################
        # Step 3 - Weighted sum of hidden states, by the attention scores
        ##################################################################

        # multiply each hidden state with the attention weights
        weighted = torch.mul(inputs, scores.unsqueeze(-1).expand_as(inputs))

        # sum the hidden states
        representations = weighted.sum(1).squeeze()

        return representations, scores


class Model(nn.Module):
    def __init__(self, embeddings=None, num_embeddings=0, out_size=1, RNNType='LSTM', **kwargs):
        """
        Define the layer of the model and perform the initializations
        of the layers (wherever it is necessary)
        Args:
            embeddings (numpy.ndarray): the 2D ndarray with the word vectors
            out_size ():
        """
        super(Model, self).__init__()
        embed_dim = kwargs.get("embed_dim", 310)
        embed_finetune = kwargs.get("embed_finetune", False)
        embed_dropout = kwargs.get("embed_dropout", 0.)

        encoder_size = kwargs.get("encoder_size", 128)
        encoder_layers = kwargs.get("num_layers")
        encoder_dropout = kwargs.get("encoder_dropout", 0.)

        attention = kwargs.get("attention", False)
        attention_layers = kwargs.get("attention_layers", 1)
        attention_dropout = kwargs.get("attention_dropout", 0.)
        attention_activation = kwargs.get("attention_activation", "tanh")
        self.attention_context = kwargs.get("attention_context", False)

        ########################################################

        # define the embedding layer, with the corresponding dimensions
        if embeddings is not None:
            self.embedding = EmbeddingLayer(num_embeddings=embeddings.shape[0], embedding_dim=embeddings.shape[1],
                                            embeddings=embeddings, dropout=embed_dropout, trainable=embed_finetune)
        else:
            if num_embeddings == 0:
                raise ValueError("if an embedding matrix is not passed, "
                                 "`num_embeddings` cant be zero.")
            self.embedding = EmbeddingLayer(num_embeddings=num_embeddings, embedding_dim=embed_dim,
                                            dropout=embed_dropout, trainable=True)

        if embeddings is not None:
            encoder_input_size = embeddings.shape[1]
        else:
            encoder_input_size = embed_dim

        #################################################################
        # Encoders
        #################################################################
        self.encoder = RNNLayer(input_size=encoder_input_size, rnn_size=encoder_size, num_layers=encoder_layers,
                                dropout=encoder_dropout, RNNType=RNNType)

        feat_size = self.encoder.feature_size

        self.feature_size = feat_size

        if attention:
            att_size = feat_size
            if self.attention_context:
                context_size = self.encoder.feature_size
                att_size += context_size

            self.attention = AttentionLayer(att_size, layers=attention_layers, dropout=attention_dropout,
                                            non_linearity=attention_activation, batch_first=True)

        self.linear = nn.Linear(in_features=self.feature_size, out_features=out_size)

    def init_embeddings(self, weights, trainable):
        self.embedding.weight = nn.Parameter(torch.from_numpy(weights), requires_grad=trainable)

    @staticmethod
    def _mean_pooling(x, lengths):
        sums = torch.sum(x, dim=1)
        _lens = lengths.view(-1, 1).expand(sums.size(0), sums.size(1))
        means = sums / _lens.float()
        return means

    @staticmethod
    def _sort_by(lengths):
        """
        Sort batch data and labels by length.
        Useful for variable length inputs, for utilizing PackedSequences
        Args:
            lengths (nn.Tensor): tensor containing the lengths for the data

        Returns:
            - sorted lengths Tensor
            - sort (callable) which will sort a given iterable
                according to lengths
            - unsort (callable) which will revert a given iterable to its
                original order

        """
        batch_size = lengths.size(0)

        sorted_lengths, sorted_idx = lengths.sort()
        _, original_idx = sorted_idx.sort(0, descending=True)
        reverse_idx = torch.linspace(batch_size - 1, 0, batch_size).long()

        reverse_idx = reverse_idx.to(DEVICE)

        sorted_lengths = sorted_lengths[reverse_idx]

        def sort(iterable):
            if len(iterable.shape) > 1:
                return iterable[sorted_idx.data][reverse_idx]
            else:
                return iterable

        def unsort(iterable):
            if len(iterable.shape) > 1:
                return iterable[reverse_idx][original_idx][reverse_idx]
            else:
                return iterable

        return sorted_lengths, sort, unsort

    def forward(self, x, lengths):

        lengths, sort, unsort = self._sort_by(lengths)
        x = sort(x)

        embeddings = self.embedding(x)

        attentions = None
        outputs, last_output = self.encoder(embeddings, lengths)

        if hasattr(self, 'attention'):
            if self.attention_context:
                context = self._mean_pooling(outputs, lengths)
                context = context.unsqueeze(1).expand(-1, outputs.size(1), -1)
                outputs = torch.cat([outputs, context], -1)

            representations, attentions = self.attention(outputs, lengths)

            if self.attention_context:
                representations = representations[:, :context.size(-1)]
        else:
            representations = last_output

        # unsort
        representations = unsort(representations)
        if attentions is not None:
            attentions = unsort(attentions)

        logits = self.linear(representations)

        return logits, attentions
