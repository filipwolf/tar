import argparse
import sys

import numpy as np

from dataloaders.task3 import parse
from logger.training import predict_ensemble
from model.params import TASK3_A, TASK3_B
from utils.train import define_trainer, model_training

sys.path.append('.')
sys.path.append('..')
sys.path.append('../../')
sys.path.append('../../../')

##############################################################################
# Command line Arguments
##############################################################################

parser = argparse.ArgumentParser(description='test', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--task', help="choose subtask a or b", default="b")
parser.add_argument('--mode', help='choose "char" or "word"', default="word")
args = parser.parse_args()
print(args)

if args.task not in ["a", "b"]:
    raise ValueError("Invalid task!")
if args.mode not in ["char", "word"]:
    raise ValueError("Invalid mode!")

# select config by args.task
if args.task == "a":
    model_config = TASK3_A
else:
    model_config = TASK3_B

model_config["embed_dim"] = 310
model_config["embed_finetune"] = False
model_config["embed_dropout"] = 0.1
model_config["num_layers"] = 2

# set the operation mode

##############################################################

# load the dataset and split it in train and val sets
X_train, y_train = parse(task=args.task, dataset="train")
X_test, y_test = parse(task=args.task, dataset="gold")

datasets = {
    "train": (X_train, y_train),
    "gold": (X_test, y_test),
}

name = "_".join([model_config["name"], model_config["token_type"]])

param_dict = {}

model_config["attention"] = "ntua"
model_config["token_type"] = "word"

values_f11 = []
values_acc1 = []
values_prec1 = []
values_rec1 = []

values_f12 = []
values_acc2 = []
values_prec2 = []
values_rec2 = []

values_f1 = []
values_acc = []
values_prec = []
values_rec = []

for i in range(5):

    print("Iteration: " + str(i))

    model_config["attention_activation"] = "tanh"

    model1 = define_trainer("clf", config=model_config, name=name, datasets=datasets, monitor="gold", RNNType='LSTM')

    model_training(model1, model_config["epochs"], checkpoint=False)

    # model.log_training(name, model_config["token_type"])
    scores = {k: v for k, v in model1.scores.items() if k != "loss"}
    values_f11.append(max(scores["f1"]["gold"]))
    values_prec1.append(max(scores["precision"]["gold"]))
    values_rec1.append(max(scores["recall"]["gold"]))
    values_acc1.append(max(scores["acc"]["gold"]))

    model_config["attention_activation"] = "sigmoid"

    model2 = define_trainer("clf", config=model_config, name=name, datasets=datasets, monitor="gold", RNNType='LSTM')

    model_training(model2, model_config["epochs"], checkpoint=False)

    scores = {k: v for k, v in model2.scores.items() if k != "loss"}
    values_f12.append(max(scores["f1"]["gold"]))
    values_prec2.append(max(scores["precision"]["gold"]))
    values_rec2.append(max(scores["recall"]["gold"]))
    values_acc2.append(max(scores["acc"]["gold"]))

    for k, v in model1.loaders.items():
        if k == "gold":

            avg_loss, (y, y_pred) = predict_ensemble(model1, model2, model1.task, args.task)

            scores = {name: metric(y, y_pred) for name, metric in model1.metrics.items()}

            print("\t{:6s} - ".format(k), end=" ")
            for name, value in scores.items():
                print(name, '{:.4f}'.format(value), end=", ")
            print(" Loss:{:.4f}".format(avg_loss))

            scores["loss"] = avg_loss

            _dataset_names = list(model1.loaders.keys())
            _metric_names = list(model1.metrics.keys()) + ["loss"]
            scores2 = {metric: {d: [] for d in _dataset_names} for metric in _metric_names}

            for name, value in scores.items():
                scores2[name][k].append(value)

            # model.log_training(name, model_config["token_type"])
            scores2 = {k: v for k, v in scores2.items() if k != "loss"}
            values_f1.append(max(scores2["f1"][k]))
            values_prec.append(max(scores2["precision"][k]))
            values_rec.append(max(scores2["recall"][k]))
            values_acc.append(max(scores2["acc"][k]))

param_dict["Attention1"] = [np.mean(values_f11), np.std(values_f11), np.mean(values_acc1), np.mean(values_prec1),
                            np.mean(values_rec1)]

param_dict["Attention2"] = [np.mean(values_f12), np.std(values_f12), np.mean(values_acc2), np.mean(values_prec2),
                            np.mean(values_rec2)]

param_dict["Attention"] = [np.mean(values_f1), np.std(values_f1), np.mean(values_acc), np.mean(values_prec),
                           np.mean(values_rec)]

for i in param_dict.keys():
    print(i + ": " + str(param_dict[i]))
