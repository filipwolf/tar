"""
This file contains functions with logic that is used in almost all models,
with the goal of avoiding boilerplate code (and bugs due to copy-paste),
such as training pipelines.
"""
import math
import os

import numpy
import torch
from scipy.stats import pearsonr
from sklearn.metrics import f1_score, recall_score, accuracy_score, precision_score, jaccard_score
from torch.utils.data import DataLoader

from config import BASE_PATH, DEVICE
from logger.training import class_weigths, Checkpoint, EarlyStop, Trainer
from modules.nn.dataloading import WordDataset, CharDataset
from modules.nn.layers import Model
from utils.load_embeddings import load_word_vectors
from utils.nlp import twitter_preprocess


def load_datasets(datasets, train_batch_size, eval_batch_size, token_type, preprocessor=None, params=None,
                  word2idx=None, label_transformer=None):
    if params is not None:
        name = "_".join(params) if isinstance(params, list) else params
    else:
        name = None

    loaders = {}
    if token_type == "word":
        if word2idx is None:
            raise ValueError

        if preprocessor is None:
            preprocessor = twitter_preprocess()

        print("Building word-level datasets...")
        for k, v in datasets.items():
            _name = "{}_{}".format(name, k)
            dataset = WordDataset(v[0], v[1], word2idx, name=_name,
                                  preprocess=preprocessor,
                                  label_transformer=label_transformer)
            batch_size = train_batch_size if k == "train" else eval_batch_size
            loaders[k] = DataLoader(dataset, batch_size, shuffle=True,
                                    drop_last=True)

    elif token_type == "char":
        print("Building char-level datasets...")
        for k, v in datasets.items():
            _name = "{}_{}".format(name, k)
            dataset = CharDataset(v[0], v[1], name=_name,
                                  label_transformer=label_transformer)
            batch_size = train_batch_size if k == "train" else eval_batch_size
            loaders[k] = DataLoader(dataset, batch_size, shuffle=True,
                                    drop_last=True)

    else:
        raise ValueError("Invalid token_type.")

    return loaders


def load_embeddings(model_conf, absolute_path=False, embedding_size_auto_detect=None):
    if not absolute_path:
        word_vectors = os.path.join(
            BASE_PATH, "embeddings",
            "{}.txt".format(model_conf["embeddings_file"]))
    else:
        '''Absolute Path.'''
        word_vectors = model_conf["embeddings_file"]

    if embedding_size_auto_detect is not None:
        word_vectors_size = detect_embedding_dim(word_vectors)
    else:
        word_vectors_size = model_conf["embed_dim"]

    # load word embeddings
    print("loading word embeddings...")
    return load_word_vectors(word_vectors, word_vectors_size)


def detect_embedding_dim(embedding_file):
    """
    Auto detecting the dimensionality of embedding file.
    :param embedding_file:
    :return:
    """
    with open(embedding_file, 'r', encoding="utf8") as file:
        for line in file:
            return len(line.strip().split()) - 1


def get_pipeline(task, criterion=None, eval=False):
    """
    Generic classification pipeline
    Args:
        task (): available tasks
                - "clf": multiclass classification
                - "bclf": binary classification
                - "mclf": multilabel classification
                - "reg": regression
        criterion (): the loss function
        eval (): set to True if the pipeline will be used
            for evaluation and not for training.
            Note: this has nothing to do with the mode
            of the model (eval or train). If the pipeline will be used
            for making predictions, then set to True.

    Returns:

    """

    def pipeline(nn_model, curr_batch):
        # get the inputs (batch)
        inputs, labels, lengths, indices = curr_batch

        if task in ["reg", "mclf"]:
            labels = labels.float()

        inputs = inputs.to(DEVICE)
        labels = labels.to(DEVICE)
        lengths = lengths.to(DEVICE)

        outputs, attentions = nn_model(inputs, lengths)

        if eval:
            return outputs, labels, attentions, None

        if task == "bclf":
            loss = criterion(outputs.view(-1), labels.float())
        else:
            loss = criterion(outputs.squeeze(), labels)

        return outputs, labels, attentions, loss

    return pipeline


def calc_pearson(y, y_hat):
    score = pearsonr(y, y_hat)[0]
    if math.isnan(score):
        return 0
    else:
        return score


def get_metrics(task, ordinal):
    _metrics = {
        "reg": {
            "pearson": calc_pearson,
        },
        "bclf": {
            "acc": lambda y, y_hat: accuracy_score(y, y_hat),
            "precision": lambda y, y_hat: precision_score(y, y_hat,
                                                          average='macro'),
            "recall": lambda y, y_hat: recall_score(y, y_hat,
                                                    average='macro'),
            "f1": lambda y, y_hat: f1_score(y, y_hat,
                                            average='macro'),
        },
        "clf": {
            "acc": lambda y, y_hat: accuracy_score(y, y_hat),
            "precision": lambda y, y_hat: precision_score(y, y_hat,
                                                          average='macro'),
            "recall": lambda y, y_hat: recall_score(y, y_hat,
                                                    average='macro'),
            "f1": lambda y, y_hat: f1_score(y, y_hat,
                                            average='macro'),
        },
        "mclf": {
            "jaccard": lambda y, y_hat: jaccard_score(
                numpy.array(y), numpy.array(y_hat)),
            "f1-macro": lambda y, y_hat: f1_score(numpy.array(y),
                                                  numpy.array(y_hat),
                                                  average='macro'),
            "f1-micro": lambda y, y_hat: f1_score(numpy.array(y),
                                                  numpy.array(y_hat),
                                                  average='micro'),
        },
    }
    _monitor = {
        "reg": "pearson",
        "bclf": "f1",
        "clf": "f1",
        "mclf": "jaccard",
    }
    _mode = {
        "reg": "max",
        "bclf": "max",
        "clf": "max",
        "mclf": "max",
    }

    if ordinal:
        task = "reg"

    metrics = _metrics[task]
    monitor = _monitor[task]
    mode = _mode[task]

    return metrics, monitor, mode


def define_trainer(task, config, name, datasets, monitor, RNNType, ordinal=False, label_transformer=None,
                   disable_cache=False, absolute_path=False, embedding_size_auto_detect=None):

    # Load embeddings
    word2idx, idx2word, embeddings = load_embeddings(config, absolute_path, embedding_size_auto_detect)

    # DATASET
    # construct the pytorch Datasets and Dataloaders
    loaders = load_datasets(datasets, train_batch_size=config["batch_train"], eval_batch_size=config["batch_eval"],
                            token_type=config["token_type"], params=None if disable_cache else name, word2idx=word2idx,
                            label_transformer=label_transformer)

    # MODEL
    # Define the model that will be trained and its parameters
    classes = len(set(loaders["train"].dataset.labels))
    out_size = 1 if classes == 2 else classes

    num_embeddings = None

    model = Model(embeddings=embeddings, num_embeddings=num_embeddings, out_size=out_size, **config, RNNType=RNNType)
    model.to(DEVICE)
    print(model)

    weights = class_weigths(loaders["train"].dataset.labels, to_pytorch=True)
    weights = weights.to(DEVICE)

    # Loss function and optimizer
    if out_size > 2:
        criterion = torch.nn.CrossEntropyLoss(weight=weights)
    else:
        criterion = torch.nn.BCEWithLogitsLoss()

    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adam(parameters, weight_decay=config["weight_decay"])

    # Trainer
    pipeline = get_pipeline("bclf" if out_size == 1 else "clf", criterion)

    metrics, monitor_metric, mode = get_metrics(task, ordinal)

    checkpoint = Checkpoint(name=name, model=model, model_conf=config, monitor=monitor, keep_best=True, scorestamp=True,
                            metric=monitor_metric, mode=mode, base=config["base"])
    early_stopping = EarlyStop(metric=monitor_metric, mode=mode, monitor=monitor, patience=config["patience"])

    trainer = Trainer(model=model, loaders=loaders, task=task, config=config, optimizer=optimizer, pipeline=pipeline,
                      metrics=metrics, use_exp=False, inspect_weights=False, checkpoint=checkpoint,
                      early_stopping=early_stopping)

    return trainer


def unfreeze_module(module, optimizer):
    for param in module.parameters():
        param.requires_grad = True

    optimizer.add_param_group(
        {'params': list(
            module.parameters())}
    )


def model_training(model, epochs, checkpoint=False):
    print("Training...")
    for epoch in range(epochs):
        model.train()
        model.eval()

        print()

        if checkpoint:
            model.checkpoint.check()

        if model.early_stopping.stop():
            print("Early stopping...")
            break
