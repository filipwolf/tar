import os

from config import DATA_DIR


class TASK3(object):
    TASK_A = os.path.join(
        DATA_DIR,
        'task3/train/SemEval2018-T3-train-taskA_emoji.txt')
    TASK_A_TEST = os.path.join(
        DATA_DIR,
        'task3/test/SemEval2018-T3_input_test_taskA_emoji.txt')
    TASK_A_GOLD = os.path.join(
        DATA_DIR,
        'task3/gold/SemEval2018-T3_gold_test_taskA_emoji.txt')
    TASK_B = os.path.join(
        DATA_DIR,
        'task3/train/SemEval2018-T3-train-taskB_emoji.txt')
    TASK_B_TEST = os.path.join(
        DATA_DIR,
        'task3/test/SemEval2018-T3_input_test_taskB_emoji.txt')
    TASK_B_GOLD = os.path.join(
        DATA_DIR,
        'task3/gold/SemEval2018-T3_gold_test_taskB_emoji.txt')
